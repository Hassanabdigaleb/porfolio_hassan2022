import styles from "./ProjetDestop.module.css"
import Programme from "../ressources/Programme.png";
import Stagiaire from "../ressources/Stagiaire.png";
import ValidationProg from "../ressources/ValidationProg.png";
import Consulter from "../ressources/Consulter.png";
import photoProjetDestop from "../ressources/destop.jpg"

export default function ProjetDestop() {
    return <>
        <div className={styles.conteneur}>


            <div className={styles.gauche}>
                <h1>Projet Destop</h1>
                <div className={styles.entete}>
                    <img src={photoProjetDestop} alt="photodestop" />
                </div>

            </div>

        </div>


        <div className={styles.conteneurDestop}>


            <div className={styles.titre2}>
                <p id='projet1'>  Application Desktop </p>
            </div >

            <div className={styles.Texte3}>
                <li>
                    Cette application Destop permet de gérer
                    les stagiaires qui appartiennent à des programmes différents.
                </li>
                <li>
                    Pour réaliser , voilà les exigences du professeur:
                    <ul>
                        <li>	Interface : noms des composants, alignement des composants, respect des couleurs 	</li>
                        <li>	Fonctionnement complet de l’application 	</li>
                        <li>	Qualité du code écrit : noms de variables, code structuré et facile à lire …	</li>
                        <li>	Commentaires : les méthodes, les formules, tout ce qui est nécessaire à commenter 	</li>
                    </ul>
                </li>
                <li>
                    voilà quelques photos du projet.
                </li>
            </div>

            <div className={styles.Texte4}>
                <div className={styles.photoAppli}>
                    <img src={Programme} alt='programme' ></img>
                    <img src={ValidationProg} alt='valudationProg'></img>
                    <img src={Stagiaire} alt='stagiaire' ></img>
                    <img src={Consulter} alt='consulter' ></img>
                </div>
            </div>

        </div>

    </>
}