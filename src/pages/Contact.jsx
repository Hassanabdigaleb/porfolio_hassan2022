import React from "react";
import { Formik } from "formik";
import "./ContactFormik.css";
import carte1 from "../ressources/carte1.png";
import MesInformation from "../components/MesInformation";

const initialValues = {
    name: "",
    email: "",
    message: ""
};

const validate = (values) => {
    let errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

    if (!values.name) {
        errors.name = "Veuillez entrer votre nom complet";
    } else if (values.name.length < 4) {
        errors.name = "Votre nom est trop court";
    }
    if (!values.email) {
        errors.email = " Veuillez entrer votre Email";
    } else if (!regex.test(values.email)) {
        errors.email = "Email est incorrect";
    }
    if (!values.message) {
        errors.message = "Veuillez mettre un message";
    } else if (values.message.length < 4) {
        errors.message = "le message est court";
    }
    return errors;
};


export default function Contact() {
    return <div className="conteneur1">


                <img src={carte1} alt='carte' ></img>
                

                  



            <Formik  
                initialValues={initialValues}
                validate={validate}
                onSubmit={(values, { setSumitting, resetForm }) => {
                    setTimeout(() => {
                        alert(JSON.stringify("hhhh" && values, null, 2));
                        resetForm();
                        setSumitting(false)
                    }, 300);
                }}
            >
                {(formik) => {
                    const {
                        values,
                        handleChange,
                        handleSubmit,
                        errors,
                        touched,
                        handleBlur,
                        isValid,
                        dirty
                    } = formik;
                    return (
                        <div className="conteneur">
                            <h1>Envoyer un message</h1>
                            <form onSubmit={handleSubmit}>

                                <div className="form-row">
                                    <label htmlFor="name">Nom</label>
                                    <input
                                        type="name"
                                        name="name"
                                        id="name"
                                        placeholder="Nom"
                                        value={values.name}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={
                                            errors.name && touched.name ? "input-error" : null
                                        }
                                    />
                                    {errors.name && touched.name && (
                                        <span className="error">{errors.name}</span>
                                    )}
                                </div>

                                <div className="form-row">
                                    <label htmlFor="email">Email</label>
                                    <input
                                        type="email"
                                        name="email"
                                        id="email"
                                        placeholder="courriel@domaine.com"
                                        value={values.email}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={
                                            errors.email && touched.email ? "input-error" : null
                                        }
                                    />
                                    {errors.email && touched.email && (
                                        <span className="error">{errors.email}</span>
                                    )}
                                </div>

                                <div className="form-row">
                                    <label htmlFor="message">Message</label>
                                    <textarea
                                        rows="8"
                                        cols="80"
                                        placeholder="Mettre un message "
                                        type="message"
                                        name="message"
                                        id="message"

                                        value={values.message}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={
                                            errors.C && touched.message ? "input-error" : null
                                        }
                                    />
                                    {errors.message && touched.message && (
                                        <span className="error">{errors.message}</span>
                                    )}
                                </div>

                                <button
                                    type="submit"
                                    className={!(dirty && isValid) ? "disabled-btn" : "disabled-btn1"}
                                    disabled={!(dirty && isValid)}
                                >
                                    Envoyer le message
                                </button>
                            </form>
                        </div>
                    );
                }}
            </Formik>
            );
            <MesInformation  className="info"/>
        </div>
    
}

