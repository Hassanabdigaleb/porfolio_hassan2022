import types from "./Accueil.module.css"
import photoInfor from "../ressources/photoInfor.jpg"
import MesInformation from "../components/MesInformation"
import { useState } from "react"


export default function Accueil(props) {

    const [visible, setvisible] = useState(!!props.visible)
    const contentTegg = () => {
        setvisible(!visible)
    }

    return <div className={types.Accueil}>
        <div className={types.AccueilEnTete}>
            <h1>Hi, je suis un programmeur et  bientôt en formation en intelligence artificielle</h1>
            <p>« Qui ne progresse pas chaque jour, recule chaque jour. » Citation de Confucius</p>
            

                <p> En avril 2022, j'ai terminé une formation en programmation informatique à la Cité Collégiale d’Ottawa.</p>

                <p>Au cours de ma formation théorique et des nombreux travaux pratiques réalisés avec des professeurs très compétents et exigeants j’ai acquis des connaissances et de solides compétences parmi les langages de programmation les plus populaires sur le marché tel que JavaScript, HTML, CSS, Java, c#, React.Js, Node JS et SQL. En plus, mes diverses expériences professionnelles avant de venir au Canada m’ont permis de développer mon esprit équipe, des atouts qui sont aujourd’hui mes plus grandes forces.
                </p>
                <p>Mon objectif est d’être intégré dans un environnement de travail où je peux utiliser ma formation en développement pour créer et implanter des logiciels et aussi de concevoir et développer des sites web.</p>
                <div className={types.AccueilButton}>
                    <button onClick={contentTegg}>Mes informations</button>
                    <svg className={visible ? types.inverse : ""} width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 0c6.623 0 12 5.377 12 12s-5.377 12-12 12-12-5.377-12-12 5.377-12 12-12zm0 1c6.071 0 11 4.929 11 11s-4.929 11-11 11-11-4.929-11-11 4.929-11 11-11zm5.247 8l-5.247 6.44-5.263-6.44-.737.678 6 7.322 6-7.335-.753-.665z" /></svg>
                    

                </div>



                <div>
                    {visible ?
                        <div>
                            {<MesInformation />}
                        </div>
                        : null
                    }
                </div>







        </div>

        <div className={types.AccueilPhoto}>
            <img src={photoInfor} alt="photoinfo" />
        </div >

        <div className={types.AccueilProjet}>

            <h1>Projets réalisés</h1>

            <div className={types.projets}>
                <div className={types.projet}>
                    <h4>Projet 1</h4>
                    <p className={types.description}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut dolores itaque temporibus repellat natus vel fuga ex odit doloremque nam quis atque, eos ducimus pariatur unde nesciunt modi, accusamus beatae.</p>
                    <p className={types.date}>le 2/02/2022</p>
                </div>
                <div className={types.projet}>
                    <h4>Projet 1</h4>
                    <p className={types.description} >Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut dolores itaque temporibus repellat natus vel fuga ex odit doloremque nam quis atque, eos ducimus pariatur unde nesciunt modi, accusamus beatae.</p>
                    <p className={types.date}>le 2/02/2022</p>
                </div>
                <div className={types.projet}>
                    <h4>Projet 1</h4>
                    <p className={types.description}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut dolores itaque temporibus repellat natus vel fuga ex odit doloremque nam quis atque, eos ducimus pariatur unde nesciunt modi, accusamus beatae.</p>
                    <p className={types.date}>le 2/02/2022</p>
                </div>
                <div className={types.projet}>
                    <h4>Projet 1</h4>
                    <p className={types.description}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut dolores itaque temporibus repellat natus vel fuga ex odit doloremque nam quis atque, eos ducimus pariatur unde nesciunt modi, accusamus beatae.</p>
                    <p className={types.date}>le 2/02/2022</p>
                </div>
                <div className={types.projet}>
                    <h4>Projet 1</h4>
                    <p className={types.description}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut dolores itaque temporibus repellat natus vel fuga ex odit doloremque nam quis atque, eos ducimus pariatur unde nesciunt modi, accusamus beatae.</p>
                    <p className={types.date}>le 2/02/2022</p>
                </div>
            </div>
        </div >
    </div>
}