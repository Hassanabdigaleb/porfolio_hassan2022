import MesInformation from "../components/MesInformation";
import types from "./Apropos.module.css"

import photoCompetence from "../ressources/competence.jpg";
import photoAptitude from "../ressources/aptitude.jpg";
import photoEtude from "../ressources/etude.jpg";
import photoexperience from "../ressources/experience.jpg"


export default function Apropos() {
    return <>
        <MesInformation></MesInformation>


        <div className={types.Apropos}>
            <div className={types.lienMemePage}>
                <h1>À propos </h1>
                <a href='#MesCompetences'>Mes Compétences </a>
                <a href='#MesAptitudes'>Mes Aptitudes  </a>
                <a href='#MesEtudes'>Mes Etudes et Formations</a>
                <a href='#MesExperiences'>Mes Experiences</a>
            </div>


            <div className={types.Competence}>
                <div className={types.Mots}>Mes Compétences
                    <div className={types.Photo1}>
                        <img src={photoCompetence} alt="Competences" />
                    </div>

                </div>

                <div className={types.Detail}>
                    <ul>
                        <li>
                            Conception  des applications informatiques
                            à l'aide de différents langages de programmation, selon le type d'application désiré.
                        </li>
                        <li>
                            Création des applications
                            commerciales avec différents langages comme Java, C#, Visual Basic et COBOL.

                        </li>
                        <li>
                            Utilisation de réseau local et élaboration des applications autonomes graphiques (Java, C++, Visual Basic) ainsi que des applications internet (HTML, VBScript, Javascript, PHP, ASP).
                        </li>
                        <li>
                            Utilisation aussi des bases de données Access, Microsoft SQL Server et MySQL.
                        </li>
                        <li>
                            Création, testing, installation et maintenance des applications.
                        </li>
                    </ul>
                </div >



            </div>





            <div className={types.Competence}>

                <div id='MesAptitudes' className={types.Mots}>  Mes Aptitudes
                    <div className={types.Photo1}>
                        <img src={photoAptitude} alt="Competences" />

                    </div>
                </div>


                <div className={types.Detail}>
                    <ul>
                        <li>
                            Souci du détail et sens de l'innovation
                        </li>
                        <li>
                            Patience, sociabilité et persévérance
                        </li>
                        <li>
                            Esprit logique et scientifique
                        </li>
                        <li>
                            Curiosité et goût d'apprendre
                        </li>
                        <li>
                            Aptitudes pour la résolution de problèmes et le travail d'équipe
                        </li>
                        <li>
                            Plus de dix ans d’expérience en service à la clientèle
                        </li>
                        <li>
                            Intérêt pour l’implication sociale auprès des aînés
                        </li>
                        <li>
                            Grande autonomie dans l’exécution de tâches
                        </li>
                        <li>
                            Respect de la confidentialité des clients
                        </li>
                    </ul>
                </div>


            </div>

            <div className={types.Competence}>

                <div id='MesEtudes' className={types.Mots}>  Mes Etudes et Formations
                    <div className={types.Photo1}>
                        <img src={photoEtude} alt="Competences" />

                    </div>

                </div>




                <div className={types.Detail}>
                    <ul>
                        <li>
                            2020 -2022
                            <ul>
                                <li>
                                    Diplôme d’études collégiales de l’Ontario : Programmation informatique
                                </li>
                                <li>
                                    La Cité collégiale (Ottawa)
                                </li>
                                <li>
                                    En cours de formation (En quatre étapes)
                                </li>
                            </ul>
                        </li>
                        <li>
                            1995-1996
                            <ul>
                                <li>
                                    Licence mention biologie générale et science de la terre
                                </li>
                                <li>
                                    Université de Pau et des pays de l’Adour (France)
                                </li>
                                <li>
                                    Date d’obtention du diplôme : juin 1996
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div>

            <div className={types.Competence}>

                <div id='MesExperiences' className={types.Mots}>  Mes Expériences
                    <div className={types.Photo1}>
                        <img src={photoexperience} alt="Competences" />
                    </div>

                </div>

                <div className={types.Detail}>
                    <h2>   Expérience de travail : hors canada </h2>
                    <ul>
                        <li>
                            2016 -2019
                            <ul>
                                <li>
                                    Responsable administratif d’une École privée de soutien scolaire
                                </li>
                            </ul>
                        </li>
                        <li>
                            2015 -2016
                            <ul>
                                <li>
                                    Comptable restaurant
                                    <ul>
                                        <li>
                                            L’enregistrement quotidien des opérations comptables
                                        </li>
                                        <li>
                                            La gestion de la paie des salariés
                                        </li>

                                        <li>
                                            L’enregistrement des opérations comptables dans une base de données Excel
                                        </li>
                                        <li>
                                            La préparation un bilan mensuel
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                            <li>
                                2010 -2012
                                <ul>
                                    <li>
                                        Aide Comptable
                                    </li>
                                </ul>
                            </li>
                            <li>
                                1997 -2020
                                <ul>
                                    <li>
                                        Enseignant en science de la vie et de la terre
                                    </li>
                                </ul>
                            </li>
                        </li>
                    </ul>
                </div >

            </div>
        </div >













    </>
}