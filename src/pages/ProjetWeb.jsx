import styles from "./ProjetWeb.module.css"


import Interphase from "../ressources/InterphaseGraphique.png";
import validation from "../ressources/validatio3.png";
import Graphique from "../ressources/Graphique.png";
import Menu1 from "../ressources/menu1.png";
import Menu2 from "../ressources/menu2.png";
import Panier from "../ressources/Panier.png";
import Commandes from "../ressources/Commandes.png";
import connexion from "../ressources/connexion.png";
import Inscription from "../ressources/Inscription.png";
import photoweb from "../ressources/projet-site-web.png";

export default function ProjetWeb() {
    return <>
        <div className={styles.conteneur}>
            <div className={styles.gauche}>
                <h1>Projet web</h1>
                <div className={styles.entete}>
                    <img src={photoweb} alt="photoweb" />
                </div>
                
            </div>
            <div className={styles.droite}>
                <ul>
                    <li>
                        Projet1:
                    </li><ul>
                        <li>
                            Sandages:
                        </li>

                    </ul>
                </ul>
                <ul>
                    <li>
                        Projet2:
                    </li><ul>
                        <li>
                            Restaurant
                        </li>

                    </ul>
                </ul>
                <ul>
                    <li>
                        Projet3:
                    </li><ul>
                        <li>
                            Gestion d'une école
                        </li>

                    </ul>
                </ul>

            </div>

        </div>
        <div className={styles.conteneurProjetWeb}>

            <div className={styles.titre1}>
                <p id='projet1' className={styles.titreweb}>  Les sondages </p>
            </div >

            <div className={styles.Texte1}>
                <h3>programmation Web client</h3>
                <li>
                    Mon premier projet web consistait à aider un professeur enseignant en administration des affaires qui utilisait des questionnaires papiers dans ses cours pour montrer aux étudiants quel sorte d’employé ou de gestionnaire ils sont.
                </li>
                <li>
                    Avec la pandémie du covid 19, l'utilisation des questionnaires papier est devenu impossible, il a décidé de convertir les tests dans un format Web pour que les étudiants puissent les faire facilement de chez eux.
                </li>
                <li>
                    Pour réaliser , voilà les exigences du professeur:
                    <ul>
                        <li>	Interface graphique responsive contenant tous les éléments nécessaires et respectant le guide de style	</li>
                        <li>	Validation du questionnaire et affichage des erreurs de validation 	</li>
                        <li>	Calcul des résultats du questionnaire 	</li>
                        <li>	Affichage des résultats et graphique 	</li>
                        <li>	Bouton de téléchargement en PDF et bouton d’impression 	</li>
                    </ul>
                </li>
                <li>
                    voilà quelques photos du projet.
                </li>
            </div>

            <div className={styles.Texte1}>

                <div className={styles.graphique1}>
                    <img src={Interphase} alt='interphase' className={styles.interp} ></img>
                    <img src={validation} alt='interphase' ></img>
                    <img src={Graphique} alt='interphase' ></img>

                </div>
            </div>

            <div className={styles.titre1}>
                <p id='projet2' className={styles.titreweb}>  Restaurant  </p>
            </div >

            <div className={styles.Texte1}>
                <h3>programmation web serveur </h3>
                <li>
                    Mon deuxième projet consistait à réaliser un restaurant en ligne à partir d’un site Web et la livraison de nourriture se fait à domicile.
                </li>
                <li>
                    Pour réaliser la plateforme de commande, voilà les exigences du professeur:
                </li>

                <div className={styles.projet2}>
                    <div className={styles.partie}>
                        <h3>Partie1</h3>
                        <ul>
                            <li>	Page de visionnement du menu	</li>
                            <ul>
                                <li>	Génération de la page avec un modèle	</li>
                                <li>	Programmation de routes 	</li>
                                <li>	Validation des données	</li>
                                <li>	Utilisation de la base de données 	</li>
                            </ul>
                            <li>	Page de révision de commande/panier	</li>
                            <ul>
                                <li>	Génération de la page avec un modèle 	</li>
                                <li>	Programmation de routes 	</li>
                                <li>	Validation des données 	</li>
                                <li>	Utilisation de la base de données 	</li>
                            </ul>
                            <li>	Page de visionnement de toutes le commandes	</li>
                            <ul>
                                <li>	Génération de la page avec un modèle 	</li>
                                <li>	Programmation de routes 	</li>
                                <li>	Validation des données	</li>
                                <li>	Utilisation de la base de données 	</li>
                            </ul>
                            <li>	Autre</li>
                            <ul><li>	Bonnes pratiques</li>
                            </ul>
                        </ul>
                    </div>

                    <div className={styles.partie}>
                        <h3>Partie2</h3>
                        <ul>
                            <li>	Page de création de compte</li>
                            <ul>
                                <li>	Affichage de la page	</li>
                                <li>	Programmation et utilisation des routes	</li>
                            </ul>
                            <li>	Page de connexion à un compte	</li>
                            <ul>
                                <li>	Affichage de la page 	</li>
                                <li>	Programmation et utilisation des routes 	</li>
                            </ul>
                            <li>	Modification de la page de visionnement du menu	</li>
                            <ul>
                                <li>	Ajouter et gérer les droits d’utilisateurs 	</li>
                            </ul>
                            <li>	Modification de la page de révision de commande/panier	</li>
                            <ul>
                                <li>	Ajouter et gérer les droits d’utilisateurs 	</li>
                            </ul>
                            <li>	Modification de la page de visionnement de toutes les  commandes	</li>
                            <ul>
                                <li>	Ajouter et gérer les droits d’utilisateurs 	</li>
                                <li>	Ajouter les modifications en temps réel 	</li>
                            </ul>
                            <li>	Autre	</li>
                            <ul>
                                <li>	Sécuriser avec HTTPS 	</li>
                                <li>	Bonnes pratiques	</li>
                            </ul>
                        </ul>
                    </div>
                </div >

                <div className={styles.photoResto}>
                    <img src={Menu1} alt='menu' className={styles.photRest}></img>
                    <img src={Inscription} alt='inscription' className={styles.photRest}></img>
                    <img src={connexion} alt='connexion' className={styles.photRest}></img>
                    <img src={Menu2} alt='menu' className={styles.photRest}></img>
                    <img src={Panier} alt='Panier' className={styles.photRest}></img>
                    <img src={Commandes} alt='commandes' className={styles.photRest}></img>
                </div>
            </div >





        </div>




    </>
}