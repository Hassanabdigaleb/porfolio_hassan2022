import photoCompetence from "../ressources/competences.png"
import types from "./CompAptiForExp.module.css"
export default function Competence() {
    return <div className={types.Competence}>
        <div className={types.competenceMots}>Mes Compétences</div>

        <div className={types.competenceDetail}>
            <ul>
                <li>
                    Conception  des applications informatiques
                    à l'aide de différents langages de programmation, selon le type d'application désiré.
                </li>
                <li>
                    Création des applications
                    commerciales avec différents langages comme Java, C#, Visual Basic et COBOL.

                </li>
                <li>
                    Utilisation de réseau local et élaboration des applications autonomes graphiques (Java, C++, Visual Basic) ainsi que des applications internet (HTML, VBScript, Javascript, PHP, ASP).
                </li>
                <li>
                    Utilisation aussi des bases de données Access, Microsoft SQL Server et MySQL.
                </li>
                <li>
                    Création, testing, installation et maintenance des applications.
                </li>
            </ul>
            </div >

            <div className={types.competencePhoto}>
                <img src={photoCompetence} alt="Competences" />
            </div>

        </div>
    
}