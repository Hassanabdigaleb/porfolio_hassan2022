import styles from "./MesInformation.module.css"
import hassan from "../ressources/hassan.jpg"

export default function MesInformation() {

    return <div className={styles.MesInfo}>
        <div className={styles.MesInfo1}>
            <div className={styles.MesInfoTelepAdres}>

                <h3>Information Personnelle</h3>
                <div className={styles.MesInfoTelepAdres1}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2.02c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 12.55l-5.992-4.57h11.983l-5.991 4.57zm0 1.288l-6-4.629v6.771h12v-6.771l-6 4.629z" /></svg>
                    <span>hassanabdigaleb@gmail.com</span>
                </div>
                <div className={styles.MesInfoTelepAdres1}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M20 22.621l-3.521-6.795c-.008.004-1.974.97-2.064 1.011-2.24 1.086-6.799-7.82-4.609-8.994l2.083-1.026-3.493-6.817-2.106 1.039c-7.202 3.755 4.233 25.982 11.6 22.615.121-.055 2.102-1.029 2.11-1.033z" /></svg>
                    <span>613 xxxx xx xx</span>
                </div>
                <div className={styles.MesInfoTelepAdres1}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M20 7.093v-5.093h-3v2.093l3 3zm4 5.907l-12-12-12 12h3v10h7v-5h4v5h7v-10h3zm-5 8h-3v-5h-8v5h-3v-10.26l7-6.912 7 6.99v10.182z" /></svg>
                    <span>OTTAWA</span>
                </div>
            </div>
        </div>

        <div className={styles.MesInfo1}>
            <h2> Hassan Abdi Galeb </h2>
            <h2> Programmeur </h2>
        </div >
        <div className={styles.MesInfo1}>

            <img src={hassan} alt='hassan' className={styles.ha} />
        </div>



    </div>
}
