import types from "./Header.module.css"
import Nav from "./Nav"
import hassanPhoto from "../ressources/hassan.jpg"
export default function Header(props) {
    return <header className={types.header}>
        <div className={types.profile}>
            <img src={hassanPhoto} alt="photoHassan" />
            <h1> Hassan Abdi Galeb</h1>
        </div>


        <div>
            bonjour, {props.nom} {props.prenom}
        </div>
        <Nav ChangePage={props.ChangePage} />
    </header>
}
